import { redirect } from "@sveltejs/kit";
import type { LayoutServerLoad } from "./$types";


export const load: LayoutServerLoad = async ({ locals: { supabase } }) => {

    // try and get a user, if there is none then redirect to the sign in page
    const { data } = await supabase.auth.getUser();

    if (!data.user) {
        redirect(302, "/auth/signin");
    }


    const { data: userProfile } = await supabase.from("UserProfile").select("name").eq("id", data.user!.id).single();


    return {
        userProfile
    }
}