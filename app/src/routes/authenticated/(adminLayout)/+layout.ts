import type { LayoutLoad } from "./$types";


export const load: LayoutLoad = async ({ fetch, data }) => {

    return {
        userProfile: data.userProfile
    }
}
