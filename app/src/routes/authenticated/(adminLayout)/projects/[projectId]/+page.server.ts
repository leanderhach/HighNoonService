import type { PageServerLoad } from "../$types";


export const load: PageServerLoad = async ({ params, locals: { supabase } }) => {

    const { data: project } = await supabase.from("Project").select("*").eq("id", params.projectId).single();
    const { data: tokens } = await supabase.from("Token").select("*").eq("projectId", params.projectId);

    console.log(project)
    return {
        project,
        tokens
    }
}