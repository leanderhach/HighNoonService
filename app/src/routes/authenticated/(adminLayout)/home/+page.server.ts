import type { PageServerLoad } from "./$types";

export const load: PageServerLoad = async ({ locals: { supabase } }) => {

    const { data: projects } = await supabase.from("Project").select("name, description");

    return {
        projects
    };
}