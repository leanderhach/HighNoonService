import { redirect } from '@sveltejs/kit'
import type { LayoutServerLoad } from './$types'

export const load: LayoutServerLoad = async ({ locals: { supabase } }) => {

    // try and get a user, if there is none then redirect to the sign in page
    const { data } = await supabase.auth.getUser();

    if (!data) {
        redirect(302, '/auth/sign-in');
    }
}