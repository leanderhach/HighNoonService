import { goto } from "$app/navigation";
import { redirect } from "@sveltejs/kit";

export async function load({ locals: { supabase }}) {
    const { data, error } = await supabase.from("UserProfile").select();

    if (data && data.length > 0) {
        return redirect(302, "/authenticated/home");
    }
}