import { browser } from "$app/environment";
import { writable } from "svelte/store";

function createLocalStore(key:string, startValue: string | number | boolean | object | null | undefined) {
    // Use the value from localStorage if it exists, otherwise use the startValue
    const storedValue =  browser ? localStorage.getItem(key) : "";
    const initialValue = storedValue ? JSON.parse(storedValue) : startValue;

    // Create a Svelte store with the initial value
    const store = writable(initialValue);

    // Subscribe to changes in the store value
    store.subscribe((value) => {
        // Update localStorage when the store value changes
        if (browser) localStorage.setItem(key, JSON.stringify(value));
    });

    return store;
}


export const userSignupEmailStore = createLocalStore("userSignupEmail", "");


